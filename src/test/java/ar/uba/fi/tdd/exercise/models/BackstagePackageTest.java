package ar.uba.fi.tdd.exercise.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BackstagePackageTest {

    @Test
    public void updateQualityWithsellInMinorThan6() {
        BackstagePackage backstage = new BackstagePackage("Backstage", 5, 10);

        backstage.updateQuality();
        Assertions.assertEquals(13, backstage.getItem().quality);
    }

    @Test
    public void updateQualityWithsellInMinorThan11() {
        BackstagePackage backstage = new BackstagePackage("Backstage", 10, 10);

        backstage.updateQuality();
        Assertions.assertEquals(12, backstage.getItem().quality);
    }

    @Test
    public void updateQualityBeforeShow() {
        BackstagePackage backstage = new BackstagePackage("Backstage", 10, 10);

        backstage.updateAfterSellInAndQuality();
        Assertions.assertEquals(0, backstage.getItem().quality);
    }

    @Test
    public void updateSellIn() {
        BackstagePackage backstage = new BackstagePackage("Backstage", 10, 10);

        backstage.updateSellIn();
        Assertions.assertEquals(9, backstage.getItem().sellIn);
    }

}
