package ar.uba.fi.tdd.exercise.models;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ItemTest {

   @Test
   public void testItemToString() {
       Item item = new Item();
       item.quality = 10;
       item.sellIn = 10;
       item.name = "Backstage";

       assertEquals("Backstage, 10, 10", item.toString());
   }
}