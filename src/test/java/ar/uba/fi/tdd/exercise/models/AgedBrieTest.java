package ar.uba.fi.tdd.exercise.models;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AgedBrieTest {

    @Test
    public void createAgedBrieAndUpdateValues_shouldBeUpdated() {
        AgedBrie agedBrie = new AgedBrie("Aged Brie", 10, 10);

        agedBrie.updateQuality();
        agedBrie.updateSellIn();
        agedBrie.updateAfterSellInAndQuality();
        Assertions.assertEquals( 10, agedBrie.getItem().quality);
        Assertions.assertEquals( 9, agedBrie.getItem().sellIn);
    }

    @Test
    public void createAgedBrieAndUpdateValues_sellinshouldntBeUpdated() {
        AgedBrie agedBrie = new AgedBrie("Aged Brie", 10, 0);

        agedBrie.updateQuality();
        agedBrie.updateSellIn();
        agedBrie.updateAfterSellInAndQuality();
        Assertions.assertEquals( 1, agedBrie.getItem().quality);
        Assertions.assertEquals( 9, agedBrie.getItem().sellIn);
    }


}
