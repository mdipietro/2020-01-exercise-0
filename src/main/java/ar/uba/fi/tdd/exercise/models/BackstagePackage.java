package ar.uba.fi.tdd.exercise.models;

public class BackstagePackage extends NormalInventoryItem {

    public BackstagePackage(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {

        if (this.canModifyQuality()) {
            if (this.item.sellIn < 6) {
                this.item.quality = this.item.quality + 3;
            } else if (this.item.sellIn < 11) {
                this.item.quality = this.item.quality + 2;
            }

            if (this.item.quality >= 50) {
                this.item.quality = 50;
            }
        }
    }

    @Override
    public void updateSellIn() {
        this.item.sellIn = this.item.sellIn - 1;
    }

    @Override
    public void updateAfterSellInAndQuality() {
        this.item.quality = 0;
    }
}