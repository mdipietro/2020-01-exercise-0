package ar.uba.fi.tdd.exercise;

import ar.uba.fi.tdd.exercise.models.InventoryItem;

class GildedRose {
    InventoryItem[] items;

    public GildedRose(InventoryItem[] items) {
        this.items = items;
    }

    // update the quality of the emements
    public void updateQuality() {
        // for each item
        for (InventoryItem item : items) {
            item.updateQuality();
            item.updateSellIn();
            item.updateAfterSellInAndQuality();
        }
    }
}
