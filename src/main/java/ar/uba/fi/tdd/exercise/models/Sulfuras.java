package ar.uba.fi.tdd.exercise.models;

public class Sulfuras extends LegendaryInventoryItem {
    public Sulfuras(String name, int sellIn) {

        super(name, sellIn, 80);
    }
}
