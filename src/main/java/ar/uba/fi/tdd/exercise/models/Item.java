package ar.uba.fi.tdd.exercise.models;

public class Item {

    // Item name
    public String name;

    // Denotes the number of days we have to sell the item
    public int sellIn;

    // Denotes how valuable the item is
    public int quality;


    // Shows the Item representations
    @Override
    public String toString() {

        return this.name + ", " + this.sellIn + ", " + this.quality;
    }

}
