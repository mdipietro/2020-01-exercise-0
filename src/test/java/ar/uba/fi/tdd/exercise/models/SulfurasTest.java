package ar.uba.fi.tdd.exercise.models;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SulfurasTest {

    @Test
    public void createSulfurasItem_QualityShouldntChange() {
        Sulfuras sulfuras = new Sulfuras("Sulfuras", 10);

        int qualityAfterUpdate = sulfuras.getItem().quality;
        sulfuras.updateQuality();

        assertThat(qualityAfterUpdate).isEqualTo(sulfuras.getItem().quality);
    }

    @Test
    public void createSulfurasItem_SellInShouldDecrease() {
        Sulfuras sulfuras = new Sulfuras("Sulfuras", 10);
        sulfuras.updateSellIn();

        assertThat(9).isEqualTo(sulfuras.getItem().sellIn);
    }
}