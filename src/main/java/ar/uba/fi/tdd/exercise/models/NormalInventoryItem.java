package ar.uba.fi.tdd.exercise.models;

public abstract class NormalInventoryItem extends InventoryItem {

    public NormalInventoryItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        if (this.canModifyQuality() && this.canModifyQuality()) {
            this.item.quality = this.item.quality - 1;
        }
    }

    @Override
    public void updateSellIn() {
        this.item.sellIn = this.item.sellIn - 1;
    }

    @Override
    public void updateAfterSellInAndQuality() {
        if (this.isSellByDatePassed()) {
            this.item.quality -= 1;
        }
    }

    protected boolean canModifyQuality() {
        if (this.item.quality > 0 && this.item.quality < 50) {
            return true;
        }

        return false;
    }

    protected boolean canIncreaseQuality() {
        if (this.item.quality == 50) {
            return false;
        }
        return true;
    }

    private boolean isSellByDatePassed() {
        if (this.item.sellIn <= 0) {
            return true;
        }

        return false;
    }

}
