package ar.uba.fi.tdd.exercise.models;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ConjuredTest {

    @Test
    public void createConjuredItem_QualityShouldChangeAsDoubleAsNormal() {
        Conjured conjured = new Conjured("Conjured", 10, 10);

        conjured.updateQuality();
        assertThat(8).isEqualTo(conjured.getItem().quality);
    }

    @Test
    public void createSulfurasItem_SellInShouldDecrease() {
        Conjured conjured = new Conjured("Conjured", 10, 1);

        conjured.updateQuality();
        assertThat(0).isEqualTo(conjured.getItem().quality);
    }
}