package ar.uba.fi.tdd.exercise;

import ar.uba.fi.tdd.exercise.models.AgedBrie;
import ar.uba.fi.tdd.exercise.models.InventoryItem;
import ar.uba.fi.tdd.exercise.models.Sulfuras;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

    @Test
    public void createGildedRoseWithTwoSulfurasAndBothWhereUpdated() {
        InventoryItem[] items = new InventoryItem[]{
                new Sulfuras("Sulfuras1", 30),
                new Sulfuras("Sulfuras2", 30)
        };

        GildedRose app = new GildedRose(items);
        app.updateQuality();

        assertThat("Sulfuras1").isEqualTo(app.items[0].getItem().name);
        assertThat(29).isEqualTo(app.items[0].getItem().sellIn);

        assertThat("Sulfuras2").isEqualTo(app.items[1].getItem().name);
        assertThat(29).isEqualTo(app.items[0].getItem().sellIn);
    }

}
