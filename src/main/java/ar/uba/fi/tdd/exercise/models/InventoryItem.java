package ar.uba.fi.tdd.exercise.models;

public abstract class InventoryItem {
    Item item;

    InventoryItem(String name, int sellIn, int quality) {
        this.item = new Item();
        this.item.name = name;
        this.item.sellIn = sellIn;
        this.item.quality = quality;
    }

    public Item getItem() {
        return this.item;
    }

    public abstract void updateQuality();

    public abstract void updateSellIn();

    public abstract void updateAfterSellInAndQuality();
}
