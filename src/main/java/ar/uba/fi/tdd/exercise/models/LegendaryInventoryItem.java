package ar.uba.fi.tdd.exercise.models;

public abstract class LegendaryInventoryItem extends InventoryItem {

    public LegendaryInventoryItem(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
    }

    @Override
    public void updateSellIn() {

        this.item.sellIn = this.item.sellIn - 1;
    }

    @Override
    public void updateAfterSellInAndQuality() {
    }
}
