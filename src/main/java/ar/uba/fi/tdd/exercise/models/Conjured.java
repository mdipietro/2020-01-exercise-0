package ar.uba.fi.tdd.exercise.models;

public class Conjured extends NormalInventoryItem {
    public Conjured(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        if (this.canModifyQuality()) {
            this.item.quality = this.item.quality - 2;
        } else {
            this.item.quality = 0;
        }
    }

    @Override
    public boolean canModifyQuality() {
        if (this.item.quality - 2 >= 0) {
            return true;
        }

        return false;
    }
}
