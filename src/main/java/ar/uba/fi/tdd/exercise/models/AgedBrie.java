package ar.uba.fi.tdd.exercise.models;

public class AgedBrie extends NormalInventoryItem {

    public AgedBrie(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateSellIn() {
        this.item.sellIn = this.item.sellIn - 1;
    }

    @Override
    public void updateAfterSellInAndQuality() {
        if (this.canIncreaseQuality()) {
            this.item.quality = this.item.quality + 1;
        }
    }
}
